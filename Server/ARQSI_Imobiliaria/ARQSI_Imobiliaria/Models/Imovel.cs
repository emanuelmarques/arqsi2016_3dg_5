﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ARQSI_Imobiliaria.Models
{
    public class Imovel
    {
        [Key]
       // [ForeignKey("anuncio")]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("tipoDeImovel")]
        public int tipoDeImovelID { get; set; }
        public virtual TipoDeImovel tipoDeImovel { get; set; }

        [ForeignKey("localizacao")]
        public int localizacaoID { get; set; }
        public virtual Localizacao localizacao { get; set; }

        public decimal area { get; set; }

        //public virtual Anúncio anuncio { get; set; }
    }
}