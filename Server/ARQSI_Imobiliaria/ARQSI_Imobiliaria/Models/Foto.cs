﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ARQSI_Imobiliaria.Models
{
    public class Foto
    {
        [Key]
        public int ID { get; set; }

        public string url { get; set; }

        [ForeignKey("anuncio")]
        public int anuncioID { get; set; }
        public virtual Anúncio anuncio { get; set; }
    }
}