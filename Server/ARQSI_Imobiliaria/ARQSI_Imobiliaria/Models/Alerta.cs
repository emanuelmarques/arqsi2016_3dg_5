﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ARQSI_Imobiliaria.Models
{
    public class Alerta
    {


        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string descricao { get; set; }
        public decimal areaMinima { get; set; }
        public decimal areaMaxima { get; set; }
        public virtual float precoMinimo { get; set; }
        public virtual float precoMaximo { get; set; }
        public DateTime dataInicio { get; set; }
        public DateTime dataFim { get; set; }


        [ForeignKey("tipoAnuncio")]
        public int tipoAnuncioID { get; set; }
        public virtual TipoDeAnuncio tipoAnuncio { get; set; }

        [ForeignKey("tipoImovel")]
        public int tipoDeImovelID { get; set; }
        public virtual TipoDeImovel tipoImovel { get; set; }

        [ForeignKey("localizacao")]
        public int localizacaoID { get; set; }
        public virtual Localizacao localizacao { get; set; }

        [ForeignKey("owner")]
        public virtual string ownerID { get; set; }
        public virtual ApplicationUser owner { get; set; }

    }
}