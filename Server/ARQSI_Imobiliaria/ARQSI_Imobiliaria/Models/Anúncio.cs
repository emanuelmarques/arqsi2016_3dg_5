﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ARQSI_Imobiliaria.Models
{
    public class Anúncio
    {   
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("tipoAnuncio")]
        public int tipoAnuncioID { get; set; }
        public virtual TipoDeAnuncio tipoAnuncio { get; set; }

        [ForeignKey("imovel")]
        public int imovelID { get; set; }
        public virtual Imovel imovel { get; set; }

        [ForeignKey("owner")]
        public virtual string ownerID { get; set; }
        public virtual ApplicationUser owner { get; set; }

        public virtual float preco { get; set; }

        public virtual IList<Foto> foto { get; set; }
    }
}