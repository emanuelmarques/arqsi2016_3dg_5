﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ARQSI_Imobiliaria.Startup))]
namespace ARQSI_Imobiliaria
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
