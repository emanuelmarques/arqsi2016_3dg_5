﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ARQSI_Imobiliaria.Models;

namespace ARQSI_Imobiliaria.Controllers
{
    public class TipoDeImovelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoDeImovels
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var tipoDeImoveis = db.TipoDeImoveis.Include(t => t.subTipo);
            return View(tipoDeImoveis.ToList());
        }

        // GET: TipoDeImovels/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDeImovel tipoDeImovel = db.TipoDeImoveis.Find(id);
            if (tipoDeImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoDeImovel);
        }

        // GET: TipoDeImovels/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            
            ViewBag.subTipoID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao");
            SelectList list = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao");
            foreach (TipoDeImovel tipo in db.TipoDeImoveis)
            {
                foreach (TipoDeImovel vbTipo in ViewBag.subTipoId)
                {
                    if(tipo.tipoImovelID == vbTipo.subTipoID)
                    {
                    }
                }
            }

            return View();
        }

        // POST: TipoDeImovels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TipoDeImovel tipoDeImovel)
        {
            if (ModelState.IsValid)
            {
                db.TipoDeImoveis.Add(tipoDeImovel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.subTipoID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", tipoDeImovel.subTipoID);
            return View(tipoDeImovel);
        }

        // GET: TipoDeImovels/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDeImovel tipoDeImovel = db.TipoDeImoveis.Find(id);
            if (tipoDeImovel == null)
            {
                return HttpNotFound();
            }
            ViewBag.subTipoID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", tipoDeImovel.subTipoID);
            return View(tipoDeImovel);
        }

        // POST: TipoDeImovels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TipoDeImovel tipoDeImovel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoDeImovel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.subTipoID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", tipoDeImovel.subTipoID);
            return View(tipoDeImovel);
        }

        // GET: TipoDeImovels/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDeImovel tipoDeImovel = db.TipoDeImoveis.Find(id);
            if (tipoDeImovel == null)
            {
                return HttpNotFound();
            }
            return View(tipoDeImovel);
        }

        // POST: TipoDeImovels/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoDeImovel tipoDeImovel = db.TipoDeImoveis.Find(id);
            db.TipoDeImoveis.Remove(tipoDeImovel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
