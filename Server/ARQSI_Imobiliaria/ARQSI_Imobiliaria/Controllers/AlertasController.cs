﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ARQSI_Imobiliaria.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ARQSI_Imobiliaria.Controllers
{
    public class AlertasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected UserManager<ApplicationUser> userManager { get; set; }
        
        public AlertasController()
        {
            this.userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }

        // GET: Alertas
        [Authorize(Roles = "Cliente")]
        public ActionResult Index()
        {
            var alertas = db.Alertas.Include(a => a.localizacao).Include(a => a.tipoAnuncio);
            return View(alertas.ToList());
        }

        // GET: Alertas/Details/5
        [Authorize(Roles = "Cliente")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            var owner_id = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (!alerta.ownerID.Equals(owner_id))
            {
                return HttpNotFound("O utilizador não está autorizado");
            }
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // GET: Alertas/Create
        [Authorize(Roles = "Cliente")]
        public ActionResult Create()
        {
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID");
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao");
            return View();
        }

        // POST: Alertas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Cliente")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Alerta alerta)
        {
            alerta.ownerID = System.Web.HttpContext.Current.User.Identity.GetUserId();
            alerta.owner = this.userManager.FindById(alerta.ownerID);
            if (ModelState.IsValid)
            {
                db.Alertas.Add(alerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao", alerta.tipoAnuncioID);
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID", alerta.tipoDeImovelID);
            return View(alerta);
        }

        // GET: Alertas/Edit/5
        [Authorize(Roles = "Cliente")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            var owner_id = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (!alerta.ownerID.Equals(owner_id))
            {
                return HttpNotFound("O utilizador não está autorizado");
            }
            if (alerta == null)
            {
                return HttpNotFound();
            }
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao", alerta.tipoAnuncioID);
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID", alerta.tipoDeImovelID);
            return View(alerta);
        }

        // POST: Alertas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Cliente")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                alerta.localizacaoID = alerta.localizacao.ID;
                db.Entry(alerta.localizacao).State = EntityState.Modified;
                alerta.ownerID = alerta.owner.Id;
                db.Entry(alerta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao", alerta.tipoAnuncioID);
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID", alerta.tipoDeImovelID);
            return View(alerta);
        }

        // GET: Alertas/Delete/5
        [Authorize(Roles = "Cliente")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alertas/Delete/5
        [Authorize(Roles = "Cliente")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alerta alerta = db.Alertas.Find(id);
            db.Alertas.Remove(alerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
