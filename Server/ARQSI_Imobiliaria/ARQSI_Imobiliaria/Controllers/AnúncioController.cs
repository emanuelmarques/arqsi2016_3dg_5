﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ARQSI_Imobiliaria.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ARQSI_Imobiliaria.Controllers
{
    public class AnúncioController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected UserManager<ApplicationUser> userManager { get; set; }

        public AnúncioController()
        {
            this.userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
        }

        // GET: Anúncio
        [Authorize(Roles = "Cliente")]
        public ActionResult Index()
        {
            var anúncios = db.Anúncios.Include(a => a.imovel).Include(a => a.tipoAnuncio);
            return View(anúncios.ToList());
        }

        // GET: Anúncio/Details/5
        [Authorize(Roles = "Cliente")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anúncio anúncio = db.Anúncios.Find(id);
            var owner_id = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (!anúncio.ownerID.Equals(owner_id))
            {
                return HttpNotFound("O utilizador não está autorizado");
            }
            if (anúncio == null)
            {
                return HttpNotFound();
            }
            return View(anúncio);
        }

        // GET: Anúncio/Create
        [Authorize(Roles = "Cliente")]
        public ActionResult Create()
        {
            ViewBag.imovelID = new SelectList(db.Imoveis, "ID", "ID");
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID");
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao");
            return View();
        }

        // POST: Anúncio/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Cliente")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Anúncio anúncio)
        {
            
            anúncio.ownerID = System.Web.HttpContext.Current.User.Identity.GetUserId();
            anúncio.owner = this.userManager.FindById(anúncio.ownerID);
            if (ModelState.IsValid)
            {
                db.Anúncios.Add(anúncio);
                db.Imoveis.Add(anúncio.imovel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.imovelID = new SelectList(db.Imoveis, "ID", "ID", anúncio.imovelID);
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID", anúncio.imovel.tipoDeImovelID);
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao", anúncio.tipoAnuncioID);
            return View(anúncio);
        }

        // GET: Anúncio/Edit/5
        [Authorize(Roles = "Cliente")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anúncio anúncio = db.Anúncios.Find(id);
            var owner_id = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (!anúncio.ownerID.Equals(owner_id))
            {
                return HttpNotFound("O utilizador não está autorizado");
            }
            if (anúncio == null)
            {
                return HttpNotFound();
            }
            ViewBag.imovelID = new SelectList(db.Imoveis, "ID", "ID", anúncio.imovelID);
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID", anúncio.imovel.tipoDeImovelID);
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao", anúncio.tipoAnuncioID);
            return View(anúncio);
        }

        // POST: Anúncio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Cliente")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Anúncio anúncio)
        {
            if (ModelState.IsValid)
            {
                anúncio.imovelID = anúncio.imovel.ID;
                anúncio.imovel.localizacaoID = anúncio.imovel.localizacao.ID;
                db.Entry(anúncio.imovel).State = EntityState.Modified;
                Localizacao loc = db.Localizacaos.Find(anúncio.imovel.localizacaoID);
                db.Entry(anúncio.imovel.localizacao).State = EntityState.Modified;
                anúncio.ownerID = anúncio.owner.Id;
                db.Entry(anúncio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.imovelID = new SelectList(db.Imoveis, "ID", "ID", anúncio.imovelID);
            ViewBag.tipoDeImovelID = new SelectList(db.TipoDeImoveis, "tipoImovelID", "designacao", "subTipoID", anúncio.imovel.tipoDeImovelID);
            ViewBag.tipoAnuncioID = new SelectList(db.TipoDeAnuncios, "ID", "descricao", anúncio.tipoAnuncioID);
            return View(anúncio);
        }

        // GET: Anúncio/Delete/5
        [Authorize(Roles = "Cliente")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Anúncio anúncio = db.Anúncios.Find(id);
            var owner_id = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (!anúncio.ownerID.Equals(owner_id))
            {
                return HttpNotFound("O utilizador não está autorizado");
            }
            if (anúncio == null)
            {
                return HttpNotFound();
            }
            return View(anúncio);
        }

        // POST: Anúncio/Delete/5
        [Authorize(Roles = "Cliente")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Anúncio anúncio = db.Anúncios.Find(id);
            db.Anúncios.Remove(anúncio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
