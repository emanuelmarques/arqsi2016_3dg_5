function createTextBox(name,id){
    var box = document.createElement("input");
    box.type="text";
    box.name = name;
    box.id = id;
    return box;
}

function createCheckBox(name, value, id){
    var checkbox = document.createElement("input");
    checkbox.type="checkbox";
    checkbox.name = name;
    checkbox.value = value;
    checkbox.id = id;
    return checkbox;
}
