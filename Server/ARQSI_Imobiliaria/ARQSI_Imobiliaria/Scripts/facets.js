function facetsDataCallBack(){
    if(!isConnectionReady(connectionObject))
        return;
    facetsList = connectionObject.responseXML.getElementsByTagName("faceta");
    renderTableHeaders(); //renders table headers from facets data.
    startFacetsCalls(); //starts facet info requests
}

function renderTableHeaders(){
    //renders table headers on tableHeadRow
    for (facet of facetsList) {
        var tableHeader = document.createElement("th");
        tableHeader.appendChild(document.createTextNode(facet.childNodes[0].nodeValue));
        tableHeadRow.appendChild(tableHeader);
    }
}

function startFacetsCalls(){
    for(facet of facetsList){
        makeFacetInfoCall(facet.childNodes[0].nodeValue); //requests facet info
    }
}

function infoCallBack(facet, connection){
    if(!isConnectionReady(connection))
        return;

    //gets filters div to add facet
    var hostDiv = document.getElementById(FILTER_DIV_ID);
    var facetElement = document.createElement("div");
    //parse request response
    var detailedData = JSON.parse(connection.responseText);
    detailedInfo[facet] = detailedData;
    //checks if this facet is "discreto" ou "contínuo" and acts accordingly
    if(detailedData[DISCREET] == DISCREET){
        rendersDiscreteFacet(facet,facetElement);
    }else{
        renderContinuousFacet(facet,facetElement);
    }
    hostDiv.appendChild(facetElement);
}

function rendersDiscreteFacet(facet,facetElement){
    // create the checkbox to hide/show the options of this facet
    var facetInput = createCheckBox(facet,facet,FACET_CHECKBOX_ID+facet);
    facetElement.appendChild(facetInput);
    facetElement.appendChild(document.createTextNode(facet));

    if(detailedInfo[facet][SEMANTICS] == PICTURE){
        facetInput.onchange = function(){onPictureCheckChange(facetInput);};
    }else{
        //calls for facet options
        makeDiscreteFacetOptionsCall(facet, facetElement, facetInput);
    }
}

function renderContinuousFacet(facet,facetElement){
    // create the first text box
    var firstBox = createTextBox(facet,FACET_FIRSTBOX_ID+facet);
    firstBox.onchange =  function(){selectionChange();};
    // create the second text box
    var secondBox = createTextBox(facet,FACET_SECONDBOX_ID+facet);
    secondBox.onchange =  function(){selectionChange();};

    facetElement.appendChild(document.createTextNode(facet));
    facetElement.appendChild(firstBox);
    facetElement.appendChild(document.createTextNode(" to "));
    facetElement.appendChild(secondBox);

    makeContinuousFacetOptionsCall(facet);
}

function onPictureCheckChange(check){
    if(check.checked)
        pictureSelected = true;
    else
        pictureSelected = false;

    selectionChange();
}

function mainCheckSelectionChange(options_div){
    if (options_div.style.display == "none"){
        options_div.style.display = "block";
    }else{
        options_div.style.display = "none";
    }
}
