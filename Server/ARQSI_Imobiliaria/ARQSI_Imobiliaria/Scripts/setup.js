function startWidget(baseId){
    BASE_ID = baseId;
    setupMainStructure(); //sets widget main sctructure
    createXmlHttpRequestObject(); //creates connectionObject
    makeFacetsCall(); //triggers facets data loading and treatment
}

function setupMainStructure(){
    // create html structure

    var filters_div = document.createElement("div"); // the div with the search filters
    filters_div.id = FILTER_DIV_ID;

    var items_div = document.createElement("div"); // the div with the results
    items_div.id = ITEMS_DIV_ID;

    var table = document.createElement("table"); //table for result presentation
    table.id = TABLE_ID;
    // table.border="1";
    var tHead = document.createElement("thead");
    var tBody = document.createElement("tbody");
    tBody.id = TABLE_BODY_ID;
    tableHeadRow = document.createElement("tr");
    tHead.appendChild(tableHeadRow);
    table.appendChild(tHead);
    table.appendChild(tBody);

    items_div.appendChild(table);

    //adds html main structure to host page
    var host = document.getElementById(BASE_ID);
    host.appendChild(filters_div);
    host.appendChild(items_div);
}
