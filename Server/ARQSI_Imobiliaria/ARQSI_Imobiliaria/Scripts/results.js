function rebuildSearchQuery(){
    searchQuery = "imoveis.php?";
    for(facet in selectedOptions){
        if(selectedOptions[facet] != "")
            if(detailedInfo[facet][DISCREET]==CONTINUOUS){
                var min = document.getElementById(FACET_FIRSTBOX_ID +facet).value;
                var max = document.getElementById(FACET_SECONDBOX_ID + facet).value;
                //no limites defined (just add all)
                if(min == "" && max == ""){
                    searchQuery += "&" + facet + "=[" + selectedOptions[facet] + "]";
                }else{
                    //check limits
                    var validValues = checkContinuousValidValues(min,max,selectedOptions[facet]);
                    searchQuery += "&" + facet + "=[" + validValues + "]";
                }
            }else{
                searchQuery += "&" + facet + "=[" + selectedOptions[facet] + "]";
            }
    }
    console.log(searchQuery);
}

function checkContinuousValidValues(min,max,options){
    var validValues = "";
    //both min and max limits
    if(min != "" && max != ""){
        for(value of options){
            if(Number(value) >= min && Number(value) <= max){
                if(validValues == "")
                    validValues = value;
                else {
                    validValues += ","+value;
                }
            }
        }
    }else{
        //only min limit
        if(min != ""){
            for(value of options){
                if(Number(value) >= Number(min) ){
                    if(validValues == "")
                        validValues = value;
                    else {
                        validValues += ","+value;
                    }
                }
            }
        }else{
            //only max limit
            for(value of options){
                if(Number(value) <= Number(max)){
                    if(validValues == "")
                        validValues = value;
                    else {
                        validValues += ","+value;
                    }
                }
            }
        }
    }
    console.log(validValues);
    return validValues;
}

function showResults(){
      var old_tbody = document.getElementById(TABLE_BODY_ID);
      var new_tbody = document.createElement('tbody');
      new_tbody.id = TABLE_BODY_ID;
      var hasPicture;

      result = JSON.parse(connectionObject.responseText);
      for(linha in result){
          var tr = document.createElement("tr");
          hasPicture = false;
          for (coluna in result[linha]) {
              if(result[linha][coluna] != "" && result[linha][coluna] != null){
                  var td = document.createElement("td");
                  td.setAttribute("data-label",coluna);
                  if(detailedInfo[coluna][SEMANTICS] == PICTURE){
                      hasPicture = true;
                      for(foto in result[linha][coluna]){
                          var img = document.createElement("img");
                          img.src = result[linha][coluna][foto];
                          img.className = "photo";
                          td.appendChild(img);
                      }
                  }else{
                      var text = document.createTextNode(result[linha][coluna]);
                      td.appendChild(text);
                  }
                  tr.appendChild(td);
              }
          }
          if((pictureSelected && hasPicture) || (!pictureSelected))
            new_tbody.appendChild(tr);
      }

      old_tbody.parentNode.replaceChild(new_tbody, old_tbody);
     refreshFilters();
}

function refreshFilters(){
    for(facet of facetsList)
        if(detailedInfo[facet.childNodes[0].nodeValue][DISCREET] == DISCREET
            && detailedInfo[facet.childNodes[0].nodeValue][SEMANTICS] != PICTURE)
                refreshDiscreteFilters(facet.childNodes[0].nodeValue);
}

function refreshDiscreteFilters(facet){
    var options_div = document.getElementById(FACET_OPTIONS_DIV_ID+facet);
    var flag;
    var picture;
    for(option of options_div.childNodes){
        flag = false;
        for(ad of result){
            if(flag)
                break;
            for(property in ad){
                if(property == facet && ad[property]==option.id){
                    option.style.display="block";
                    flag = true;
                    break;
                }else{
                    option.style.display="none";
                }
            }
        }
    }
}
