<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Forms\ConfigForm;

class ConfigController extends AbstractActionController
{

    public function indexAction()
    {
        $form = new ConfigForm();
        $form->get('submit')->setValue('Set');
    
        session_start();
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $_SESSION['server'] = $request->getPost()['server'];
            
            return $this->redirect()->toRoute('login', array('controller'=>'login', 'action' => 'index'));
        }
        else
        {
            if(!empty($_SESSION['server']))
                $form->setAttribute("Server", $_SESSION['server']);

           return array('form' => $form);
        }
    }
}