<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\View;
use Application\Infrastructure\Services\ImoServices;
use Zend\Json\Json;
use Application\DTO\Anuncio;
use Application\Forms\AnuncioForm;

class AnunciosController extends AbstractActionController
{

    public function indexAction()
    { 
        if(!ImoServices::isLogged()){
            return $this->redirect()->toRoute('login', array('controller'=>'Login', 'action' => 'index'));
        }else{
            $anuncios = ImoServices::get('/api/An�ncio');
            $view = new ViewModel();
            $view->setVariable('listaAnuncios', $anuncios); //dar a lista
            return $view;
        }
    }
    
    public function readAction(){
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('anuncios');
        }
        if(!ImoServices::isLogged()){
            return $this->redirect()->toRoute('login', array('controller'=>'Login', 'action' => 'index'));
        }else{
            try {
                $anuncio = ImoServices::get('/api/An�ncio/' . $id);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('anuncios');
            }
            $view = new ViewModel();
            $view->setVariable('anuncio', $anuncio);
            return $view;
        }
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('anuncios');
        }
        
        if(!ImoServices::isLogged()){
            return $this->redirect()->toRoute('login', array('controller'=>'Login', 'action' => 'index'));
        }else{
            try {
                $anuncioArray = ImoServices::get('/api/An�ncio/'.$id, Json::TYPE_ARRAY);
                $anuncio = new Anuncio();
                $anuncio->exchangeArray($anuncioArray);
            } catch (\Exception $ex) {
                return $this->redirect()->toRoute('anuncios');
            }
        } 
        
        $form = new AnuncioForm();
        $form->bind($anuncio);
        $form->get('submit')->setAttribute('value', 'Edit');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($anuncio->getInputFilter());
            $form->setData($request->getPost());
            $x = 0;
            if ($form->isValid()) {
                $anuncio->preco = '5';
                $anuncio->ID= 2;
                $params = $anuncio->toString();
                ImoServices::put('/api/An�ncio/'.$id, Json::TYPE_OBJECT, $params);
                
                // Redirect to list of anuncios
               return $this->redirect()->toRoute('anuncios');
            }
             
        }
        
        return array(
            'id' => $id,
            'form' => $form
        );
    }
    
    public function deleteAction()
    {   
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('anuncios');
        }
        if(!ImoServices::isLogged()){
            return $this->redirect()->toRoute('login', array('controller'=>'Login', 'action' => 'index'));
        }else{
            $anuncio = ImoServices::delete('/api/An�ncio/'.$id);
            return $this->redirect()->toRoute('anuncios');
        }
    }
}