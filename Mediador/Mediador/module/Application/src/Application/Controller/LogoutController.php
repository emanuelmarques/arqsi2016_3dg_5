<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Infrastructure\Services\ImoServices;

class LogoutController extends AbstractActionController
{

    public function indexAction()
    {
        ImoServices::Logout();
        return $this->redirect()->toRoute('login', array('controller'=>'Login', 'action' => 'index'));
    }
}