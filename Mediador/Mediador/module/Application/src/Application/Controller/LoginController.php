<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Infrastructure\Services\ImoServices;
use Application\DTO\Credenciais;
use Application\Forms\LoginForm;

class LoginController extends AbstractActionController
{

    public function indexAction()
    {
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
        
            ImoServices::Logout();
        
            $credenciais = new Credenciais();
            $form->setInputFilter($credenciais->getInputFilter());
            $form->setData($request->getPost());
        
            if ($form->isValid()) {
                $credenciais->exchangeArray($form->getData());
                 
                if( ImoServices::Login($credenciais) )
                    return $this->redirect()->toRoute('anuncios', array('controller'=>'anuncios', 'action' => 'index'));
            }
        }
        return array('form' => $form);
    }
}