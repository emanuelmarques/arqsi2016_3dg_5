<?php
namespace Application\DTO;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class Anuncio
{
    public $ID;
    public $tipoAnuncioID;
    public $tipoAnuncio;
    public $imovelID;
    public $imovel;
    public $ownerID;
    public $owner;
    public $mediadorID;
    public $mediador;
    public $approved;
    public $preco;
    public $fotos;
    
    public $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->ID =  (!empty($data['ID'])) ? $data['ID'] : null;
        $this->tipoAnuncioID =  (!empty($data['tipoAnuncioID'])) ? $data['tipoAnuncioID'] : null;
        $this->tipoAnuncio =  (!empty($data['tipoAnuncio'])) ? $data['tipoAnuncio'] : null;
        $this->imovelID =  (!empty($data['imovelID'])) ? $data['imovelID'] : null;
        $this->imovel =  (!empty($data['imovel'])) ? $data['imovel'] : null;
        $this->ownerID =  (!empty($data['ownerID'])) ? $data['ownerID'] : null;
        $this->owner =  (!empty($data['owner'])) ? $data['owner'] : null;
        $this->mediadorID =  (!empty($data['mediadorID'])) ? $data['mediadorID'] : null;
        $this->mediador =  (!empty($data['mediador'])) ? $data['mediador'] : null;
        $this->approved =  (!empty($data['approved'])) ? $data['approved'] : null;
        $this->preco =  (!empty($data['preco'])) ? $data['preco'] : null;
        $this->fotos =  (!empty($data['foto'])) ? $data['foto'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
    public function toString(){
        return    'ID='.$this->ID.
                  '&tipoAnuncioID='.$this->tipoAnuncioID.
                  '&tipoAnuncio='.$this->tipoAnuncio.
                  '&imovelID='.$this->imovelID.
                  '&imovel='.$this->imovel.
                  '&ownerID='.$this->ownerID.
                  '&owner='.$this->owner.
                  '&mediadorID='.$this->mediadorID.
                  '&mediador='.$this->mediador.
                  '&approved='.$this->approved.
                  '&preco='.$this->preco.
                  '&foto='.$this->fotos;
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
    
            $inputFilter->add(array(
                'name'     => 'preco',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));
    
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
    
}

?>