<?php
namespace Application\Infrastructure\Services;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;

use Application\DTO\Credenciais;

class ImoServices
{
    const URL_Login = '/Token';
    
    public static function Login(Credenciais $credenciais)
    {
        if(isset($_SESSION['server'])){
            $client = new Client('http://' .$_SESSION['server'] .self::URL_Login, array(
                'adapter' => 'Zend\Http\Client\Adapter\Curl'
            ));
            $client->setMethod(Request::METHOD_POST);
            $params = 'grant_type=password&username=' . $credenciais->username .'&password=' .$credenciais->password;
            
            $len = strlen($params);
            
            $client->setHeaders(array(
                'Content-Type'   => 'application/x-www-form-urlencoded',
                'Content-Length' => $len 
            ));        
           
            $client->setRawBody($params);
            
            $response = $client->send();            
            
            $body=Json::decode($response->getBody());
            
            if(!empty($body->access_token))
            {
                session_start();
    
                $_SESSION['access_token'] = $body->access_token;
                $_SESSION['username'] = $credenciais->username;
    
                return true;
            }
            else 
                return false;
        }else{
            header('Location: /Mediador/Config/');
            exit;
        }
    }
    
    public static function Logout()
    {
        session_start();
        
        $_SESSION['username'] = null;
        $_SESSION['access_token'] = null;
    }
    
    public static function get($url,$type = Json::TYPE_OBJECT)
    {
        return ImoServices::httpConnection($url,Request::METHOD_GET,$type);
    }
    
    public static function delete($url,$type = Json::TYPE_OBJECT)
    {
        return ImoServices::httpConnection($url,Request::METHOD_DELETE,$type);
    }
    
    public static function post($url,$type = Json::TYPE_OBJECT)
    {
        return ImoServices::httpConnection($url,Request::METHOD_POST,$type);
    }
    
    public static function put($url,$type = Json::TYPE_OBJECT, $params)
    {
        return ImoServices::httpConnection($url,Request::METHOD_PUT,$type,$params);
    }
    
    public static function httpConnection($url, $method, $type, $params=null){
//         session_start();
        
        $client = new Client('http://' .$_SESSION['server'] .$url, array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl'
        ));
        
        $client->setMethod($method);
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        if($params != null){
            $len = strlen($params);
            
            $client->setHeaders(array(
                'Authorization'   => $bearer_token,
                'Content-Type'   => 'application/x-www-form-urlencoded'
            ));
            
            $client->setRawBody($params);
        }else{
            $client->setHeaders(array(
                'Authorization'   => $bearer_token,
            ));
        }
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=Json::decode($response->getBody(),$type);
        
        return $body;
    }
    
    public static function isLogged(){
        session_start();
        $bool = ($_SESSION['username'] != null);
        session_abort();
        return $bool;
    }
}

?>