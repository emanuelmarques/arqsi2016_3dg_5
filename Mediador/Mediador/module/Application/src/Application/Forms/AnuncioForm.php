<?php
namespace Application\Forms;

use Zend\Form\Form;

class AnuncioForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('anuncio');

        $this->add(array(
            'name' => 'ID',
            'type' => 'Hidden',
        ));
        
        $this->add(array(
            'name' => 'preco',
            'type' => 'Number',
            'options' => array(
                'label' => 'Preco',
            ),
        ));
        
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
?>