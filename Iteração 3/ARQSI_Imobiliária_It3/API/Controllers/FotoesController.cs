﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using ClassLibrary.Repositories;

namespace API.Controllers
{
    public class FotoesController : BaseController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Fotoes
        public IQueryable<Foto> GetFotoes()
        {
            return db.Fotoes;
        }

        // GET: api/Fotoes/5
        [ResponseType(typeof(Foto))]
        public IHttpActionResult GetFoto(int id)
        {
            Foto foto = db.Fotoes.Find(id);
            if (foto == null)
            {
                return NotFound();
            }

            return Ok(foto);
        }

        // PUT: api/Fotoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFoto(int id, Foto foto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != foto.ID)
            {
                return BadRequest();
            }

            db.Entry(foto).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FotoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Fotoes
        [ResponseType(typeof(Foto))]
        public IHttpActionResult PostFoto(Foto foto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Fotoes.Add(foto);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = foto.ID }, foto);
        }

        // DELETE: api/Fotoes/5
        [ResponseType(typeof(Foto))]
        public IHttpActionResult DeleteFoto(int id)
        {
            Foto foto = db.Fotoes.Find(id);
            if (foto == null)
            {
                return NotFound();
            }

            db.Fotoes.Remove(foto);
            db.SaveChanges();

            return Ok(foto);
        }

        private bool FotoExists(int id)
        {
            return db.Fotoes.Count(e => e.ID == id) > 0;
        }
    }
}