﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using ClassLibrary.Repositories;
using ClassLibrary.DTO;

namespace API.Controllers
{
    public class TipoDeAnunciosController : BaseController
    {
        private TipoDeAnuncioRepository db;

        public TipoDeAnunciosController() : base()
        {
            this.db = repositoryAccess.getRepository.getTipoDeAnuncioRepository;
        }

        // GET: api/TipoDeAnuncios
        public IQueryable<DTOTipoDeAnuncio> GetTipoDeAnuncios()
        {
            List<TipoDeAnuncio> lista = db.GetTiposDeAnuncio().ToList();

            List<DTOTipoDeAnuncio> ret = new List<DTOTipoDeAnuncio>();
            foreach (TipoDeAnuncio tipo in lista)
            {
                ret.Add(tipo.convertToDTO());
            }
            return ret.AsQueryable();
        }

        // GET: api/TipoDeAnuncios/5
        [ResponseType(typeof(DTOTipoDeAnuncio))]
        public IHttpActionResult GetTipoDeAnuncio(int id)
        {
            TipoDeAnuncio tipoDeAnuncio = db.GetByID(id);
            if (tipoDeAnuncio == null)
            {
                return NotFound();
            }

            return Ok(tipoDeAnuncio.convertToDTO());
        }

        // PUT: api/TipoDeAnuncios/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTipoDeAnuncio(int id, DTOTipoDeAnuncio tipoDeAnuncioDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoDeAnuncioDTO.ID)
            {
                return BadRequest();
            }

            //   db.Entry(tipoDeAnuncio).State = EntityState.Modified;
            TipoDeAnuncio tipo = tipoDeAnuncioDTO.convertToModel();
            db.Update(tipo);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoDeAnuncioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipoDeAnuncios
        [ResponseType(typeof(DTOTipoDeAnuncio))]
        public IHttpActionResult PostTipoDeAnuncio(DTOTipoDeAnuncio tipoDeAnuncioDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            TipoDeAnuncio tipoDeAnuncio = tipoDeAnuncioDTO.convertToModel();

            db.Insert(tipoDeAnuncio);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipoDeAnuncio.ID }, tipoDeAnuncio);
        }

        // DELETE: api/TipoDeAnuncios/5
        [ResponseType(typeof(TipoDeAnuncio))]
        public IHttpActionResult DeleteTipoDeAnuncio(int id)
        {
            TipoDeAnuncio tipoDeAnuncio = db.GetByID(id);
            if (tipoDeAnuncio == null)
            {
                return NotFound();
            }

            db.Delete(tipoDeAnuncio);
            db.SaveChanges();

            return Ok(tipoDeAnuncio);
        }

        private bool TipoDeAnuncioExists(int id)
        {
            return db.GetTiposDeAnuncio().Count(e => e.ID == id) > 0;
        }
    }
}