﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using ClassLibrary.UnitOfWork;
using ClassLibrary.Repositories;
using ClassLibrary.DTO;

namespace API.Controllers
{
    public class AlertasController :  BaseController
    {
        private AlertaRepository db;

        public AlertasController() : base()
        {
            this.db = repositoryAccess.getRepository.getAlertaRepository;
        }

        // GET: api/Alertas
        public IQueryable<DTOAlerta> GetAlertas()
        {
            List<Alerta> lista = db.GetAlertas().ToList();

            List<DTOAlerta> ret = new List<DTOAlerta>();
            foreach (Alerta alerta in lista)
            {
                ret.Add(alerta.convertToDTO());
            }
            return ret.AsQueryable();
        }

        // GET: api/Alertas/5
        [ResponseType(typeof(Alerta))]
        public IHttpActionResult GetAlerta(int id)
        {
            Alerta alerta = db.GetByID(id);
            if (alerta == null)
            {
                return NotFound();
            }

            return Ok(alerta.convertToDTO());
        }

        // PUT: api/Alertas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAlerta(int id, DTOAlerta DTOalerta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != DTOalerta.ID)
            {
                return BadRequest();
            }

            Alerta alerta = DTOalerta.convertToModel();

            db.Update(alerta);

            //  db.Entry(alerta).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlertaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Alertas
        [ResponseType(typeof(DTOAlerta))]
        public IHttpActionResult PostAlerta(DTOAlerta alertaDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Alerta alerta = alertaDTO.convertToModel();

            db.Insert(alerta);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = alerta.ID }, alerta);
        }

        // DELETE: api/Alertas/5
        [ResponseType(typeof(Alerta))]
        public IHttpActionResult DeleteAlerta(int id)
        {
            Alerta alerta = db.GetByID(id);
            if (alerta == null)
            {
                return NotFound();
            }

            db.Delete(alerta);
            db.SaveChanges();

            return Ok(alerta);
        }

        private bool AlertaExists(int id)
        {
            return db.GetAlertas().Count(e => e.ID == id) > 0;
        }
    }
}