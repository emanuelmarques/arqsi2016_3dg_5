﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary.UnitOfWork;

namespace API.Controllers
{
    public class BaseController: ApiController
    {
        protected UnitOfWork repositoryAccess;

        public BaseController()
        {
            this.repositoryAccess = new UnitOfWork();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.repositoryAccess.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}