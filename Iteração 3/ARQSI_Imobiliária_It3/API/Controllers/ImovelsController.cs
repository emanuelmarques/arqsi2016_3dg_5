﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using ClassLibrary.Repositories;
using ClassLibrary.DTO;

namespace API.Controllers
{
    public class ImovelsController : BaseController
    {
        private ImovelRepository db;

        public ImovelsController() : base()
        {
            this.db = repositoryAccess.getRepository.getImovelRepository;
        }

        // GET: api/Imovels
        public IQueryable<DTOImovel> GetImoveis()
        {
            List<Imovel> lista = db.GetImoveis().ToList();

            List<DTOImovel> ret = new List<DTOImovel>();
            foreach (Imovel imovel in lista)
            {
                ret.Add(imovel.convertToDTO());
            }
            return ret.AsQueryable();
        }

        // GET: api/Imovels/5
        [ResponseType(typeof(DTOImovel))]
        public IHttpActionResult GetImovel(int id)
        {
            Imovel imovel = db.GetByID(id);
            if (imovel == null)
            {
                return NotFound();
            }

            return Ok(imovel.convertToDTO());
        }

        // PUT: api/Imovels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImovel(int id, DTOImovel imovelDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != imovelDTO.ID)
            {
                return BadRequest();
            }

            //db.Entry(imovel).State = EntityState.Modified;
            Imovel imovel = imovelDTO.convertToModel();
            db.Update(imovel);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImovelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Imovels
        [ResponseType(typeof(DTOImovel))]
        public IHttpActionResult PostImovel(DTOImovel imovelDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Imovel imovel = imovelDTO.convertToModel();

            db.Insert(imovel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = imovel.ID }, imovel);
        }

        // DELETE: api/Imovels/5
        [ResponseType(typeof(Imovel))]
        public IHttpActionResult DeleteImovel(int id)
        {
            Imovel imovel = db.GetByID(id);
            if (imovel == null)
            {
                return NotFound();
            }

            db.Delete(imovel);
            db.SaveChanges();

            return Ok(imovel);
        }

        private bool ImovelExists(int id)
        {
            return db.GetImoveis().Count(e => e.ID == id) > 0;
        }
    }
}