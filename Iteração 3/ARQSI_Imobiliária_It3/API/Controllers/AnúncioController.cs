﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using ClassLibrary.Repositories;
using ClassLibrary.DTO;

namespace API.Controllers
{
    public class AnúncioController : BaseController
    {
        private AnuncioRepository db;

        public AnúncioController() : base()
        {
            this.db = repositoryAccess.getRepository.getAnuncioRepository;
        }

        // GET: api/Anúncio
        public IQueryable<DTOAnúncio> GetAnúncios()
        {
            List<Anúncio> lista = db.GetAnuncios().ToList();

            List<DTOAnúncio> ret = new List<DTOAnúncio>();
            foreach (Anúncio anuncio in lista)
            {
                ret.Add(anuncio.convertToDTO());
            }
            return ret.AsQueryable();
        }

        // GET: api/Anúncio/5
        [ResponseType(typeof(DTOAnúncio))]
        public IHttpActionResult GetAnúncio(int id)
        {
            Anúncio anúncio = db.GetByID(id);
            if (anúncio == null)
            {
                return NotFound();
            }

            DTOAnúncio anuncioDTO = anúncio.convertToDTO();

            return Ok(anuncioDTO);
        }

        // PUT: api/Anúncio/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnúncio(int id, DTOAnúncio anúncioDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != anúncioDTO.ID)
            {
                return BadRequest();
            }

            // db.Entry(anúncio).State = EntityState.Modified;
            Anúncio anuncio = anúncioDTO.convertToModel();
            db.Update(anuncio);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnúncioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Anúncio
        [ResponseType(typeof(DTOAnúncio))]
        public IHttpActionResult PostAnúncio(DTOAnúncio anúncioDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Anúncio anúncio = anúncioDTO.convertToModel();  

            db.Insert(anúncio);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = anúncio.ID }, anúncio);
        }

        // DELETE: api/Anúncio/5
        [ResponseType(typeof(Anúncio))]
        public IHttpActionResult DeleteAnúncio(int id)
        {
            Anúncio anúncio = db.GetByID(id);
            if (anúncio == null)
            {
                return NotFound();
            }

            db.Delete(anúncio);
            db.SaveChanges();

            return Ok(anúncio);
        }

        private bool AnúncioExists(int id)
        {
            return db.GetAnuncios().Count(e => e.ID == id) > 0;
        }
    }
}