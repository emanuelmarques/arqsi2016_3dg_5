namespace ClassLibrary.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ClassLibrary.Model.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ClassLibrary.Model.ApplicationDbContext";
        }

        protected override void Seed(ClassLibrary.Model.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            roleManager.Create(new IdentityRole("Admin"));
            roleManager.Create(new IdentityRole("Mediador"));
            roleManager.Create(new IdentityRole("Cliente"));

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            // create user for admin
            if (!(context.Users.Any(u => u.UserName == "admin@isep.ipp.pt")))
            {
                var userToInsert = new ApplicationUser { UserName = "admin@isep.ipp.pt" };
                userManager.Create(userToInsert, "Admin!123");
            }

            var user = userManager.FindByName("admin@isep.ipp.pt");
            userManager.AddToRole(user.Id, "Admin");


            // create user for mediador
            if (!(context.Users.Any(u => u.UserName == "mediador@isep.ipp.pt")))
            {
                var userToInsert = new ApplicationUser { UserName = "mediador@isep.ipp.pt" };
                userManager.Create(userToInsert, "Mediador!123");
            }

            user = userManager.FindByName("mediador@isep.ipp.pt");
            userManager.AddToRole(user.Id, "Mediador");


            // create user for cliente
            if (!(context.Users.Any(u => u.UserName == "cliente@isep.ipp.pt")))
            {
                var userToInsert = new ApplicationUser { UserName = "cliente@isep.ipp.pt" };
                userManager.Create(userToInsert, "Cliente!123");
            }

            user = userManager.FindByName("cliente@isep.ipp.pt");
            userManager.AddToRole(user.Id, "Cliente");

            // Create tipos de anuncio
            if (!(context.TipoDeAnuncios.Any(tipo => tipo.descricao == "Compra")))
            {
                TipoDeAnuncio compra = new Compra();
                compra.descricao = "Compra";
                context.TipoDeAnuncios.Add(compra);
            }

            if (!(context.TipoDeAnuncios.Any(tipo => tipo.descricao == "Venda")))
            {
                TipoDeAnuncio venda = new Venda();
                venda.descricao = "Venda";
                context.TipoDeAnuncios.Add(venda);

                An�ncio add = new An�ncio();
                add.tipoAnuncio = venda; add.tipoAnuncioID = venda.ID;
                Imovel imovel = new Imovel();
                TipoDeImovel tipoImovel = new TipoDeImovel(); tipoImovel.designacao = "Apartamento";
                Localizacao local = new Localizacao(); local.latitude = "20"; local.longitude = "20";
                imovel.area = 40000;
                imovel.localizacao = local; imovel.localizacaoID = local.ID;
                imovel.tipoDeImovel = tipoImovel; imovel.tipoDeImovelID = tipoImovel.tipoImovelID;
                add.imovel = imovel;
                add.owner = userManager.FindByName("cliente@isep.ipp.pt");
                add.ownerID = userManager.FindByName("cliente@isep.ipp.pt").Id;
                add.preco = 2000000;
                add.foto = new List<Foto>();

                context.An�ncios.Add(add);

            }

            if (!(context.TipoDeAnuncios.Any(tipo => tipo.descricao == "Aluguer")))
            {
                TipoDeAnuncio aluguer = new Aluguer();
                aluguer.descricao = "Aluguer";
                context.TipoDeAnuncios.Add(aluguer);
            }

            

            context.SaveChanges();
        }
    }
    
}
