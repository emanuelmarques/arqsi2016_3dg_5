﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Model;

namespace ClassLibrary.Repositories
{
    public class Repository
    {
        private ApplicationDbContext context;

        private AnuncioRepository anuncioRepository;
        private AlertaRepository alertaRepository;
        private ImovelRepository imovelRepository;
        private TipoDeAnuncioRepository tipoDeAnuncioRepository;
        

        public Repository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public AnuncioRepository getAnuncioRepository
        {
            get
            {

                if (this.anuncioRepository == null)
                {
                    this.anuncioRepository = new AnuncioRepository(context);
                }
                return this.anuncioRepository;
            }
        }

        public AlertaRepository getAlertaRepository
        {
            get
            {

                if (this.alertaRepository == null)
                {
                    this.alertaRepository = new AlertaRepository(context);
                }
                return this.alertaRepository;
            }
        }

        public ImovelRepository getImovelRepository
        {
            get
            {

                if (this.imovelRepository == null)
                {
                    this.imovelRepository = new ImovelRepository(context);
                }
                return this.imovelRepository;
            }
        }

        public TipoDeAnuncioRepository getTipoDeAnuncioRepository
        {
            get
            {

                if (this.tipoDeAnuncioRepository == null)
                {
                    this.tipoDeAnuncioRepository = new TipoDeAnuncioRepository(context);
                }
                return this.tipoDeAnuncioRepository;
            }
        }
    }
}
