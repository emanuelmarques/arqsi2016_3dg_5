﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Model;
using System.Data.Entity;

namespace ClassLibrary.Repositories
{
    public class AnuncioRepository : BaseRepository<Anúncio>
    {

        public AnuncioRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Anúncio> GetAnuncios()
        {
            return base.Get(null).ToList();
        }
    }
}
