﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Model;

namespace ClassLibrary.Repositories
{
    public class AlertaRepository : BaseRepository<Alerta>
    {

        public AlertaRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Alerta> GetAlertas()
        {
            return base.Get(null).ToList();
        }
    }
}
