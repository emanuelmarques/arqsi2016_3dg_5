﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Model;

namespace ClassLibrary.Repositories
{
    public class TipoDeAnuncioRepository : BaseRepository<TipoDeAnuncio>
    {

        public TipoDeAnuncioRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<TipoDeAnuncio> GetTiposDeAnuncio()
        {
            return base.Get(null).ToList();
        }
    }
}