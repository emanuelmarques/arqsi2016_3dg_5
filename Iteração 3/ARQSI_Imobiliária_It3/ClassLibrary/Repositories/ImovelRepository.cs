﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Model;

namespace ClassLibrary.Repositories
{
    public class ImovelRepository : BaseRepository<Imovel>
    {

        public ImovelRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Imovel> GetImoveis()
        {
            return base.Get(null).ToList();
        }
    }
}
