﻿using ClassLibrary.DTO;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model
{
    public class Anúncio
    {   
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("tipoAnuncio")]
        public int tipoAnuncioID { get; set; }
        public virtual TipoDeAnuncio tipoAnuncio { get; set; }

        [ForeignKey("imovel")]
        public int imovelID { get; set; }
        public virtual Imovel imovel { get; set; }

        [ForeignKey("owner")]
        public virtual string ownerID { get; set; }
        public virtual ApplicationUser owner { get; set; }

        [ForeignKey("mediador")]
        public virtual string mediadorID { get; set; }
        public virtual ApplicationUser mediador { get; set; }

        public virtual bool approved { get; set; } = false;



        public virtual float preco { get; set; }

        public virtual IList<Foto> foto { get; set; }

        public DTOAnúncio convertToDTO()
        {
            DTOAnúncio ret = new DTOAnúncio();
            ret.ID = this.ID;

            ret.tipoAnuncioID = this.tipoAnuncioID;

            ret.tipoAnuncio = this.tipoAnuncio.convertToDTO();

            ret.imovelID = this.imovelID;
            ret.imovel = this.imovel.convertToDTO();

            ret.ownerID = this.ownerID;
            ret.owner = this.owner;
            ret.mediadorID = this.mediadorID;
            ret.mediador = this.mediador;
            ret.approved = this.approved;
            ret.preco = this.preco;

            List<DTOFoto> dto = new List<DTOFoto>();
            foreach (Foto fotografia in foto)
            {
                DTOFoto foto = fotografia.convertToDTO();

                dto.Add(foto);

            }
            ret.foto = dto;


            return ret;
        }
    }
}