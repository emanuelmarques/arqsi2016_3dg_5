﻿using ClassLibrary.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model
{
    public class TipoDeImovel
    {
        [Key]
        //[ForeignKey("imovel")]
        public int tipoImovelID { get; set; }

        public string designacao { get; set; }

        [ForeignKey("subTipo")]
        public int ? subTipoID { get; set; }
        public virtual TipoDeImovel subTipo { get; set; }

        //public virtual Imovel imovel { get; set; }

        public DTOTipoDeImovel convertToDTO()
        {
            DTOTipoDeImovel tipoImovel = new DTOTipoDeImovel();
            tipoImovel.subTipoID = this.subTipoID;
            tipoImovel.tipoImovelID = this.tipoImovelID;
            if(this.subTipo != null)
            {
                tipoImovel.subTipo = this.subTipo.convertToDTO();
            }
            
            tipoImovel.designacao = this.designacao;

            return tipoImovel;
        }
    }
}