﻿using ClassLibrary.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model
{
    public class Localizacao
    {
        [Key]
        public int ID { get; set; }

        public string longitude { get; set; }
        public string latitude { get; set; }

        public DTOLocalizacao convertToDTO()
        {
            DTOLocalizacao local = new DTOLocalizacao();
            local.ID = this.ID;
            local.latitude = this.latitude;
            local.longitude = this.longitude;

            return local;
        }
    }
}