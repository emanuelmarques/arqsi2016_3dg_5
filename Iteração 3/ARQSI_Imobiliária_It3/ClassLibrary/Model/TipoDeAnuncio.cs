﻿using ClassLibrary.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model
{
    public abstract class TipoDeAnuncio
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string descricao { get; set; }

        public DTOTipoDeAnuncio convertToDTO()
        {
            DTOTipoDeAnuncio tipoDeAnuncio = new DTOVenda();
            tipoDeAnuncio.ID = this.ID;
            tipoDeAnuncio.descricao = this.descricao;

            return tipoDeAnuncio;
        }
    }
}