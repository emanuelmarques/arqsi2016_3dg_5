﻿using ClassLibrary.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model
{
    public class Imovel
    {
        [Key]
       // [ForeignKey("anuncio")]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("tipoDeImovel")]
        public int tipoDeImovelID { get; set; }
        public virtual TipoDeImovel tipoDeImovel { get; set; }

        [ForeignKey("localizacao")]
        public int localizacaoID { get; set; }
        public virtual Localizacao localizacao { get; set; }

        public decimal area { get; set; }

        //public virtual Anúncio anuncio { get; set; }

        public DTOImovel convertToDTO()
        {
            DTOImovel imovel = new DTOImovel();
            imovel.tipoDeImovelID = this.tipoDeImovelID;
            imovel.tipoDeImovel = this.tipoDeImovel.convertToDTO();
            imovel.localizacaoID = this.localizacaoID;
            imovel.localizacao = this.localizacao.convertToDTO();
            imovel.ID = this.ID;


            return imovel;
        }
    }
}