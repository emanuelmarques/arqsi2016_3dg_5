﻿using ClassLibrary.DTO;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model
{
    public class Alerta
    {


        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string descricao { get; set; }
        public decimal areaMinima { get; set; }
        public decimal areaMaxima { get; set; }
        public virtual float precoMinimo { get; set; }
        public virtual float precoMaximo { get; set; }
        public DateTime dataInicio { get; set; }
        public DateTime dataFim { get; set; }


        [ForeignKey("tipoAnuncio")]
        public int tipoAnuncioID { get; set; }
        public virtual TipoDeAnuncio tipoAnuncio { get; set; }

        [ForeignKey("tipoImovel")]
        public int tipoDeImovelID { get; set; }
        public virtual TipoDeImovel tipoImovel { get; set; }

        [ForeignKey("localizacao")]
        public int localizacaoID { get; set; }
        public virtual Localizacao localizacao { get; set; }

        [ForeignKey("owner")]
        public virtual string ownerID { get; set; }
        public virtual ApplicationUser owner { get; set; }

        public DTOAlerta convertToDTO()
        {
            DTOAlerta ret = new DTOAlerta();
            ret.ID = this.ID;
            ret.descricao = this.descricao;
            ret.areaMaxima = this.areaMaxima;
            ret.areaMinima = this.areaMinima;
            ret.dataInicio = this.dataInicio;
            ret.dataFim = this.dataFim;
            ret.precoMaximo = this.precoMaximo;
            ret.precoMinimo = this.precoMinimo;
            ret.tipoAnuncioID = this.tipoAnuncioID;

            ret.tipoAnuncioID = this.tipoAnuncioID;
            ret.tipoAnuncio = this.tipoAnuncio.convertToDTO();

            ret.tipoDeImovelID = this.tipoDeImovelID;

            ret.tipoImovel = this.tipoImovel.convertToDTO();

            ret.localizacaoID = this.localizacaoID;

            ret.localizacao = this.localizacao.convertToDTO();

            ret.ownerID = this.ownerID;
            ret.owner = this.owner;



            return ret;
        }

    }
}