﻿using ClassLibrary.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.Model
{
    public class Foto
    {
        [Key]
        public int ID { get; set; }

        public string url { get; set; }

        [ForeignKey("anuncio")]
        public int anuncioID { get; set; }
        public virtual Anúncio anuncio { get; set; }

        public DTOFoto convertToDTO()
        {
            DTOFoto foto = new DTOFoto();
            foto.anuncio = this.anuncio.convertToDTO();
            foto.anuncioID = this.anuncioID;
            foto.ID = this.ID;
            foto.url = this.url;

            return foto;
        }
    }
}