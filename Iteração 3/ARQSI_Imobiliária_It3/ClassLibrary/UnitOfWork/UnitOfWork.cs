﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.Repositories;
using ClassLibrary.Model;

namespace ClassLibrary.UnitOfWork
{
    public class UnitOfWork : IDisposable
    {
        private Repository repository;
        private ApplicationDbContext context;
        
        public UnitOfWork()
        {
            this.context = new ApplicationDbContext();
        }

        public Repository getRepository
        {
            get
            {

                if (this.repository == null)
                {
                    this.repository = new Repository(context);
                }
                return this.repository;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
