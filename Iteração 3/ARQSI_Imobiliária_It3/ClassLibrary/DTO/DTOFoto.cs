﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO
{
    public class DTOFoto
    {
     
        public int ID { get; set; }

        public string url { get; set; }

        public int anuncioID { get; set; }
        public virtual DTOAnúncio anuncio { get; set; }

        public Foto convertToModel()
        {
            Foto foto = new Foto();
            foto.anuncio = this.anuncio.convertToModel();
            foto.anuncioID = this.anuncioID;
            foto.ID = this.ID;
            foto.url = this.url;

            return foto;
        }
    }
}