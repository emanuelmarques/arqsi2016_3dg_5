﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO
{
    public class DTOImovel
    {

        public int ID { get; set; }

        public int tipoDeImovelID { get; set; }
        public virtual DTOTipoDeImovel tipoDeImovel { get; set; }

        public int localizacaoID { get; set; }
        public virtual DTOLocalizacao localizacao { get; set; }

        public decimal area { get; set; }

        //public virtual Anúncio anuncio { get; set; }

        public Imovel convertToModel()
        {
            Imovel imovel = new Imovel();
            imovel.tipoDeImovelID = this.tipoDeImovelID;
            imovel.tipoDeImovel = this.tipoDeImovel.convertToModel();
            imovel.localizacaoID = this.localizacaoID;
            imovel.localizacao = this.localizacao.convertToModel() ;
            imovel.ID = this.ID;


            return imovel;
        }
    }
}