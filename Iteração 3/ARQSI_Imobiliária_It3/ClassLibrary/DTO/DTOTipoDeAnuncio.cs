﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO
{
    public abstract class DTOTipoDeAnuncio
    {
    
        public int ID { get; set; }

        public string descricao { get; set; }

        public TipoDeAnuncio convertToModel()
        {
            TipoDeAnuncio tipoDeAnuncio = new Venda();
            tipoDeAnuncio.ID = this.ID;
            tipoDeAnuncio.descricao = this.descricao;

            return tipoDeAnuncio;
        }
    }
}