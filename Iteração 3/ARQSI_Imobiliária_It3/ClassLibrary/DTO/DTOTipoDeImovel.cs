﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO
{
    public class DTOTipoDeImovel
    {
     
        public int tipoImovelID { get; set; }

        public string designacao { get; set; }


        public int ? subTipoID { get; set; }
        public virtual DTOTipoDeImovel subTipo { get; set; }

        //public virtual Imovel imovel { get; set; }

        public TipoDeImovel convertToModel()
        {
            TipoDeImovel tipoImovel = new TipoDeImovel();
            tipoImovel.subTipoID = this.subTipoID;
            tipoImovel.tipoImovelID = this.tipoImovelID;
            if (this.subTipo != null)
            {
                tipoImovel.subTipo = this.subTipo.convertToModel();
            }
            tipoImovel.designacao = this.designacao;

            return tipoImovel;
        }
    }
}