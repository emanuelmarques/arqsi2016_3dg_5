﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO
{
    public class DTOAnúncio
    {   

        public int ID { get; set; }

        public int tipoAnuncioID { get; set; }
        public virtual DTOTipoDeAnuncio tipoAnuncio { get; set; }

        public int imovelID { get; set; }
        public virtual DTOImovel imovel { get; set; }

        public virtual string ownerID { get; set; }
        public virtual ApplicationUser owner { get; set; }

        public virtual string mediadorID { get; set; }
        public virtual ApplicationUser mediador { get; set; }

        public virtual bool approved { get; set; } = false;

        public virtual float preco { get; set; }

        public virtual IList<DTOFoto> foto { get; set; }

        public Anúncio convertToModel()
        {
            Anúncio ret = new Anúncio();
            ret.ID = this.ID;

            ret.tipoAnuncioID = this.tipoAnuncioID;
            
            ret.tipoAnuncio = this.tipoAnuncio.convertToModel();

            ret.imovelID = this.imovelID;
            ret.imovel = this.imovel.convertToModel();

            ret.ownerID = this.ownerID;
            ret.owner = this.owner;
            ret.mediadorID = this.mediadorID;
            ret.mediador = this.mediador;
            ret.approved = this.approved;
            ret.preco = this.preco;

            List<Foto> fotos = new List<Foto>();
            foreach (DTOFoto dto in foto){
                Foto foto = dto.convertToModel();

                fotos.Add(foto);

            }
            ret.foto = fotos;


            return ret;
        }

    }
}