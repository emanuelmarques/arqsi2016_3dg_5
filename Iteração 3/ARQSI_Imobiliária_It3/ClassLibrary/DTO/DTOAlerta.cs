﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO

{
    public class DTOAlerta
    {
             
        public int ID { get; set; }

        public string descricao { get; set; }
        public decimal areaMinima { get; set; }
        public decimal areaMaxima { get; set; }
        public virtual float precoMinimo { get; set; }
        public virtual float precoMaximo { get; set; }
        public DateTime dataInicio { get; set; }
        public DateTime dataFim { get; set; }

        public int tipoAnuncioID { get; set; }
        public virtual DTOTipoDeAnuncio tipoAnuncio { get; set; }

        public int tipoDeImovelID { get; set; }
        public virtual DTOTipoDeImovel tipoImovel { get; set; }

        public int localizacaoID { get; set; }
        public virtual DTOLocalizacao localizacao { get; set; }


        public virtual string ownerID { get; set; }
        public virtual ApplicationUser owner { get; set; }

        public Alerta convertToModel()
        {
            Alerta ret = new Alerta();
            ret.ID = this.ID;
            ret.descricao = this.descricao;
            ret.areaMaxima = this.areaMaxima;
            ret.areaMinima = this.areaMinima;
            ret.dataInicio = this.dataInicio;
            ret.dataFim = this.dataFim;
            ret.precoMaximo = this.precoMaximo;
            ret.precoMinimo = this.precoMinimo;
            ret.tipoAnuncioID = this.tipoAnuncioID;

            ret.tipoAnuncioID = this.tipoAnuncioID; 
            ret.tipoAnuncio = this.tipoAnuncio.convertToModel(); 

            ret.tipoDeImovelID = this.tipoDeImovelID;
            
            ret.tipoImovel = this.tipoImovel.convertToModel();

            ret.localizacaoID = this.localizacaoID;
           
            ret.localizacao = this.localizacao.convertToModel();

            ret.ownerID = this.ownerID;
            ret.owner = this.owner;



            return ret;
        }



    }
}