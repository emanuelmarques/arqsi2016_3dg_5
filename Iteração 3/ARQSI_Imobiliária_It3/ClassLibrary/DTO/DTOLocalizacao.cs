﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClassLibrary.DTO
{
    public class DTOLocalizacao
    {
    
        public int ID { get; set; }

        public string longitude { get; set; }
        public string latitude { get; set; }

        public Localizacao convertToModel()
        {
            Localizacao local = new Localizacao();
            local.ID = this.ID;
            local.latitude = this.latitude;
            local.longitude = this.longitude;

            return local;
        }
    }
}