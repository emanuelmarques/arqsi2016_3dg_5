﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClienteIT3.Startup))]
namespace ClienteIT3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
