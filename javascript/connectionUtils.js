function createXmlHttpRequestObject() {
    var connectionObject;
    try {
        connectionObject = new XMLHttpRequest();
    } catch (e) {
        try {
            connectionObject = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            connectionObject = new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return connectionObject;
}

function isConnectionReady(connection) {
    return connection.readyState == 4 && connection.status == 200;
}

function makeFacetsCall() {
    connectionObject = createXmlHttpRequestObject();
    if (connectionObject) {
        connectionObject.open("GET", URL + "facetas.php", true);
        connectionObject.onreadystatechange = facetsDataCallBack;
        connectionObject.send(null);
    } else {
        alert("Falha no pedido! ");
    }
}

function makeFacetInfoCall(facet) {
    var connection = createXmlHttpRequestObject();
    if (connection) {
        connection.open("GET", encodeURI(URL + "tipoFaceta?faceta=" + facet), true);
        connection.onreadystatechange = function() {
            infoCallBack(facet, connection);
        }; //creates the elements for this facet filter
        connection.send(null);
    } else {
        alert("Falha no pedido!a");
    }
}

function makeDiscreteFacetOptionsCall(facet, facetElement, facetInput) {
    var connection = createXmlHttpRequestObject();
    if (connection) {
        connection.open("GET", encodeURI(URL + "valoresFaceta?faceta=" + facet), true);
        connection.onreadystatechange = function() {
            discreteOptionsCallBack(facet, facetElement, connection, facetInput);
        };
        connection.send(null);
    } else {
        alert("Falha no pedido!");
    }
}

function makeContinuousFacetOptionsCall(facet) {
    var connection = createXmlHttpRequestObject();
    if (connection) {
        connection.open("GET", encodeURI(URL + "valoresFaceta?faceta=" + facet), true);
        connection.onreadystatechange = function() {
            continuousOptionsCallBack(facet, connection);
        };
        connection.send(null);
    } else {
        alert("Falha no pedido!");
    }
}

function makeSearchCall() {
    connectionObject = createXmlHttpRequestObject();
    if (connectionObject) {
        connectionObject.open("GET", encodeURI(URL + searchQuery), true);
        connectionObject.onreadystatechange = showResults;
        connectionObject.send(null);
    } else {
        alert("Falha no pedido!");
    }
}
