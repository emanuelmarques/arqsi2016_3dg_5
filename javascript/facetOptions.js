function discreteOptionsCallBack(facet, facetElement, connection, facetInput){
    if(!isConnectionReady(connection))
        return;

    var options = JSON.parse(connection.responseText);
    facetOptionsList.facet = options;

    // the div that contains the options for this facet.
    var options_div = document.createElement("div");
    options_div.id = FACET_OPTIONS_DIV_ID + facet;
    options_div.className = "subdiv";
    options_div.style.display = "none";

    // function to hide/show when checked
    facetInput.onclick = function() {mainCheckSelectionChange(options_div);}

    //Creates options checkboxes.
    loadFacetOptions(facet, options_div, options);
    facetElement.appendChild(options_div);
}

function loadFacetOptions(facet, options_div, options){
    for (var opt_key in options) {
        if (options.hasOwnProperty(opt_key)) {
            var option = options[opt_key];
                // creates an input-checkbox element
            var checkbox_div = document.createElement("div");
            checkbox_div.id = option;
            var input = createCheckBox(facet,option, facet+" "+option);
            // to do something when the input is checked
            onChangeOption(input);
            // appends the elements
            checkbox_div.appendChild(input);
            checkbox_div.appendChild(document.createTextNode(option));
            options_div.appendChild(checkbox_div);
        }
    }
    /*for (option of options) {
        // creates an input-checkbox element
        var checkbox_div = document.createElement("div");
        checkbox_div.id = option;
        var input = createCheckBox(facet,option, facet+" "+option);
        // to do something when the input is checked
        onChangeOption(input);
        // appends the elements
        checkbox_div.appendChild(input);
        checkbox_div.appendChild(document.createTextNode(option));
        options_div.appendChild(checkbox_div);
    }*/
}

function onChangeOption(element){
    var facet = element.name;
    var option = element.value;
    element.onclick= function(){
        if(element.checked){
            if(selectedOptions[facet] == "" || selectedOptions[facet] == undefined){
                selectedOptions[facet] = option;
            }else {
                selectedOptions[facet] += ","+option;
            }
        }else{
            var aux = "";
            var array = selectedOptions[facet].split(",");
            for (var key in array) {
                if (array.hasOwnProperty(key)) {
                    var opt = array[key];
                    if(opt != option){
                        if(aux == "")
                         aux = opt;
                        else {
                            aux += ","+opt;
                        }
                    }
                }
            }
            /*or(opt of array){
                if(opt != option){
                    if(aux == "")
                        aux = opt;
                    else {
                        aux += ","+opt;
                    }
                }
            }*/
            console.log("aux "+aux);
            selectedOptions[facet] = aux;
        }
        selectionChange();
    }
}

function continuousOptionsCallBack(facet, connection){
    if(!isConnectionReady(connection))
        return;

    var options = JSON.parse(connection.responseText);
    selectedOptions[facet]=options;
}

function selectionChange(){
    rebuildSearchQuery();
    makeSearchCall();
}
